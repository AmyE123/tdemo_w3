﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemController : MonoBehaviour
{

    [SerializeField]
    Image IconImage, CurrencyImage;
    [SerializeField]
    TextMeshProUGUI CostAmount, ItemName;
    MenuItem menuItem;
    [SerializeField]
    TMP_Dropdown Dropdown;
    [SerializeField]
    int[] TimesAmount = new int[3] { 1, 2, 5 };

    private int PriceSelected;
    private int AmountSelected;
    private int ItemLevel = 0;
    public int IncrementalPriceChange = 50;
    private int currentPriceIncrease = 0; 

    public void SetupShopItem(MenuItem _menuItem)
    {
        //Debug.LogWarning("this does not set up right, variables will b empty if you don't select form cdrop down first will fix");
        menuItem = _menuItem;
        IconImage.sprite = menuItem.ItemIcon;
        CurrencyImage.sprite = menuItem.CurrencyIcon;
        CostAmount.text = menuItem.Price.ToString();
//        ItemName.text = menuItem.ItemName;
        ItemName.text = (menuItem.ItemName + ItemLevel);
        AmountSelected = TimesAmount[0];
        PriceSelected = AmountSelected * menuItem.Price;
    }


    public void CalPrice()
    {
//        Debug.Log(Dropdown.value);

        for (int i = 0; i < TimesAmount.Length; i++)
        {
            if (Dropdown.value == i)
            {
                PriceSelected = 0;
//                for (int x = 0; x < TimesAmount[i]; x++) { PriceSelected += menuItem.Price + (ItemLevel+x) * IncrementalPriceChange; }
//                for (int x = 0; x < TimesAmount[i]; x++) { PriceSelected += menuItem.Price + (ItemLevel+x) * currentPriceIncrease; }
                for (int x = 0; x < TimesAmount[i]; x++) { PriceSelected += menuItem.Price + currentPriceIncrease + (x)*IncrementalPriceChange; }
                AmountSelected = TimesAmount[i];
                CostAmount.text = PriceSelected.ToString(); 
            }
        }
    }
   
    public void BuyItem()
    {
        bool Result = CurrencyManager.Instance.TryToRemoveCurrency(PriceSelected);
        if (!Result)
        {
            return;
        }

        //add stuff 
//        Debug.Log("Buy " + menuItem.ItemName + " to whatever  Cost =" + PriceSelected.ToString());

        for (int i = 0; i < AmountSelected; i++)
        {
            menuItem.OnBought();
            ItemLevel++;
//            currentPriceIncrease += IncrementalPriceChange;
            currentPriceIncrease += IncrementalPriceChange*(ItemLevel);
            ItemName.text = (menuItem.ItemName + ItemLevel);
        }
        CalPrice();
    }
}
